package com.example.activity5part1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity
{
    Button btn1_joke1, btn2_joke2, btn3_joke3;
    TextView tv_Output;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1_joke1 = findViewById(R.id.btn1_joke1);
        btn2_joke2 = findViewById(R.id.btn2_joke2);
        btn3_joke3 = findViewById(R.id.btn3_joke3);
        tv_Output = findViewById(R.id.tv_Output);

        btn1_joke1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(MainActivity.this, "To get to the other slide", Toast.LENGTH_SHORT).show();
            }
        });

        btn2_joke2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(MainActivity.this, "Because, it ran outta juice", Toast.LENGTH_SHORT).show();
            }
        });

        btn3_joke3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tv_Output.setText("He ducked");
                //Toast.makeText(MainActivity.this, "He ducked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}